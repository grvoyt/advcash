# Laravel Advcash package

### Настройки
По умолчанию роуты для приема отключены. Включить можно через ADVCASH_DEF=true

Чтобы поставить свои роуты доступны такие методы у фасада.

$route_url - Чистая ссылка на страницу.
```php
static setSuccessRoute(string $route_url): void
static setStatusRoute(string $route_url): void
static setFailRoute(string $route_url): void
```

### Добавить в .env
```text
ADVCASH_NAME=
ADVCASH_EMAIL=
ADVCASH_PASS=
```

### Если нужен доступ к view запустить команду
```$xslt
php artisan vendor:publish --provider="Grvoyt\Advcash\AdvcashServiceProvider"
```

### Events
```text
Grvoyt\Advcash\Events\AdvcashPaymentIncome (AdvcashConfirmResponse $payment)
Grvoyt\Advcash\Events\AdvcashPaymentCancel (array $payment)
Grvoyt\Advcash\Events\AdvcashPaymentStatus (AdvcashConfirmResponse $payment)
```

### Использование

```php
$formHtml = Advcash::createBitcoinRequest(float $amount, string $order_id): string
```
