<?php

Route::post('advcash/confirm','Grvoyt\Advcash\Controllers\AdvcashController@confirm')->name('advcash.confirm');
Route::post('advcash/cancel', 'Grvoyt\Advcash\Controllers\AdvcashController@cancel_payment')->name('advcash.cancel');
Route::post('advcash/status', 'Grvoyt\Advcash\Controllers\AdvcashController@status')->name('advcash.status');
