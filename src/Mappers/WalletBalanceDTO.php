<?php


namespace Grvoyt\Advcash\Mappers;


class WalletBalanceDTO
{
	/**
	 * @access public
	 * @var double
	 */
	public $amount;
	/**
	 * @access public
	 * @var string
	 */
	public $id;
}
