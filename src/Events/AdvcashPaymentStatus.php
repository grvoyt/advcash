<?php


namespace Grvoyt\Advcash\Events;


use Grvoyt\Advcash\Mappers\AdvcashConfirmResponse;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AdvcashPaymentStatus
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	public $payment;

	public function __construct(AdvcashConfirmResponse $payment)
	{
		$this->payment = $payment;
	}
}
